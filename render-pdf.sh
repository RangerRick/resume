#!/bin/sh

set -e

TOPDIR="$(cd "$(dirname "$0")"; echo $PWD)"

if [ -z "$CHROME" ]; then
	export CHROME="/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"
fi

cd "${TOPDIR}"

RESUME_HTML="public/benjamin-reed-resume.html"
RESUME_PDF="public/benjamin-reed-resume.pdf"

if [ ! -e "${RESUME_HTML}" ] || [ "basics.json" -nt "${RESUME_HTML}" ] && [ "resume.json" -nt "${RESUME_HTML}" ]; then
	echo 'WARNING: html resume is outdated, running ./render.sh first'
	./render.sh
fi

rm -f "${RESUME_PDF}"
"$CHROME" --headless \
	--verbose \
	--disable-gpu \
	--no-sandbox \
	--run-all-compositor-stages-before-draw \
	--print-to-pdf="${RESUME_PDF}" \
	--no-pdf-header-footer \
	--print-to-pdf-print-background \
	--print-to-pdf-margin-top=0 \
	--print-to-pdf-margin-bottom=0 \
	--print-to-pdf-margin-left=0 \
	--print-to-pdf-margin-right=0 \
	"${RESUME_HTML}"

chmod 644 "${RESUME_PDF}"
