#!/bin/sh

set -e

TOPDIR="$(cd "$(dirname "$0")"; echo $PWD)"

cd "${TOPDIR}"
rm -f ./public/*
npm run hackmyresume -- build basics.json resume.json TO ./public/benjamin-reed-resume.html ./public/benjamin-reed-resume.json -t theme-pdf

chmod 644 public/*
ln -sf benjamin-reed-resume.html public/index.html
